import { BrowserModule } from '@angular/platform-browser';
import { ReactiveFormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { DynamicFormComponent } from './components/dynamic-form.component';
import { DynamicFormItemComponent } from './components/dynamic-form-item.component';
import { RouterModule } from '@angular/router';
import { FormPageComponent } from './pages/form-page.component';
import { HomePageComponent } from './pages/home-page.component';
import { NgxLoadingModule } from 'ngx-loading';
import { EntityService } from './services/entity.service';
import { FormTypeService } from './services/form-type.service';
import { HttpClientModule } from '@angular/common/http';

@NgModule({
  imports: [
    BrowserModule,
    HttpClientModule,
    ReactiveFormsModule,
    NgxLoadingModule.forRoot({}),
    RouterModule.forRoot([
      { path: '', component: HomePageComponent },
      { path: 'form', component: FormPageComponent }
    ])
  ],
  declarations: [AppComponent, HomePageComponent, FormPageComponent, DynamicFormComponent, DynamicFormItemComponent],
  providers: [EntityService, FormTypeService],
  bootstrap: [AppComponent]
})
export class AppModule {
  constructor() {
  }
}
