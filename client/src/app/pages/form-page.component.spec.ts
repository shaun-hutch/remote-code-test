import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { FormPageComponent } from './form-page.component';
import { EntityService } from '../services/entity.service';
import { FormTypeService } from '../services/form-type.service';
import { ActivatedRoute, Router } from '@angular/router';
import { of, throwError } from 'rxjs';
import { FormItemBase } from '../services/form-item-base';
import { NgxLoadingModule } from 'ngx-loading';
import { DynamicFormComponent } from '../components/dynamic-form.component';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { DynamicFormItemComponent } from '../components/dynamic-form-item.component';

describe('FormPage component', () => {
  let component: FormPageComponent;
  let fixture: ComponentFixture<FormPageComponent>;

  const mockFormType = {
    id: "5e8ac57a09376e002c363f65",
    name: "Issue Record",
    formFields: [
      {
        key: "component",
        label: "Component",
        entityTypeId: "0",
        controlType: "dropdown",
        required: true,
      },
      {
        key: "issue",
        label: "Issue",
        controlType: "textbox",
        required: true,
      }
    ]
  };

  let mockFormName;

  beforeEach(async () => {
    mockFormName = 'Mock Form Name';

    TestBed.configureTestingModule({
      imports: [
        NgxLoadingModule.forRoot({}),
        ReactiveFormsModule,
        FormsModule
      ],
      declarations: [
        FormPageComponent, 
        DynamicFormComponent, 
        DynamicFormItemComponent
      ],
      providers: [
        {
          provide: EntityService,
          useValue: {
            getEntities: () => {
              return of([
                {
                  value: "shocks",
                },
                {
                  value: "suspension"
                }
              ]);
            }
          }
        },
        {
          provide: FormTypeService,
          useValue: {
            getFormType(formName: string) {
              if (formName === 'Mock Form Name') return of(mockFormType);
              else return throwError(`${formName} is not supported`);
            }
          }
        },
        {
          provide: Router,
          useValue: { navigate: () => { } }
        },
        {
          provide: ActivatedRoute,
          useValue: {
            paramMap: of({
              get: () => {
                return mockFormName;
              }
            })
          }
        }
      ]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FormPageComponent);
    component = fixture.componentInstance;
    // fixture.detectChanges();
  });


  afterEach(() => {
    TestBed.resetTestingModule();
  })

  it('should create', () => {
    expect(component).toBeDefined();
  });

  it('should generate the correct formItems from the mock formType & entity list', (done) => {
    component.ngOnInit();
    component.formItems$
      .subscribe(val => {
        expect(component.loading).toEqual(false);
        expect(val).toEqual([
          new FormItemBase({ "key": "component", "label": "Component", "required": true, "order": 1, "controlType": "dropdown", "type": "", "metaData": {}, "entityTypeId": "0", "entities": [{ "value": "shocks" }, { "value": "suspension" }] }),
          new FormItemBase({ "key": "issue", "label": "Issue", "required": true, "order": 1, "controlType": "textbox", "type": "", "metaData": {}, "entityTypeId": null })
        ]);
        done();
      })
  });

  it('should alert the error and return back to the home page if an error occurs whilst getting formType', (done) => {
    mockFormName = 'Bad Mock FormName';
    spyOn(window, 'alert');
    spyOn(component.router, 'navigate');
    component.ngOnInit();
    component.formItems$
      .subscribe(
        val => {
          done();
        },
        err => {
          console.log(err);
          expect(component.loading).toEqual(false);
          expect(window.alert).toHaveBeenCalledWith('Bad Mock FormName is not supported');
          expect(component.router.navigate).toHaveBeenCalledWith(['']);
          done();
        });
  });
});