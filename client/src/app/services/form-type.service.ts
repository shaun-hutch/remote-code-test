import { Injectable } from '@angular/core';
import { of, throwError } from 'rxjs';
import { delay } from 'rxjs/operators';

@Injectable()
export class FormTypeService {

    getFormType(formName: string) {
        if (formName === 'Issue Record') {
            let issueRecordFormType = {
                id: "5e8ac57a09376e002c363f65",
                name: "Issue Record",
                formFields: [
                    {
                        key: "component",
                        label: "Component",
                        entityTypeId: "0",
                        controlType: "dropdown",
                        required: true,
                    },
                    {
                        key: "issue",
                        label: "Issue",
                        controlType: "textbox",
                        required: true,
                    }
                ]
            };
            return of(issueRecordFormType);
        } else {
            return throwError(formName + ' not implemented');
        }
    }
}
