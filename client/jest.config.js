// default config loaded, see https://www.npmjs.com/package/@angular-builders/jest
module.exports = {
    preset: "jest-preset-angular",
    setupFilesAfterEnv: ["<rootDir>/src/setupJest.ts"],
    roots: [
        'src'
    ]
};