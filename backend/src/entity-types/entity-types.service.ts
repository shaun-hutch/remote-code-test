import { Injectable } from '@nestjs/common';
import { CreateEntityTypeDto, EntityTypeDto, EntityDto } from './interfaces/entity-type.dto';
import { Observable, of, throwError } from 'rxjs';

@Injectable()
export class EntityTypesService {
  private readonly entityTypes: EntityTypeDto[] = [
    {
      _id: '0',
      entities: [
        {
          value: 'Brakes',
          metaData: {
            manufacturer: 'Siemens',
            dateOfManufacture: '2016-05-08T00:00:00Z',
          },
        },
        {
          value: "Shocks"
        },
      ],
    },
  ];

  create(createEntityType: CreateEntityTypeDto): Observable<EntityTypeDto> {
    const entityType: EntityTypeDto = {
      ...createEntityType,
      _id: this.entityTypes.length.toString(), //TODO implement a uuid generator
    };
    this.entityTypes.push(entityType);
    return of(entityType);
  }

  getEntities(entityTypeId: string): Observable<EntityDto[]> {
    const entityType: EntityTypeDto = this.entityTypes.find((item) => item._id === entityTypeId);
    if (entityType && entityType.entities) {
      return of(entityType.entities);
    } else if (entityType) {
      throw Error('EntitiesArrayNotFoundForEntityType');
    } else {
      throw Error('EntityTypeNotFound');
    }
  }
}
