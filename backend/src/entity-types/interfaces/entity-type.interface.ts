
export interface EntityType {
  entities?: Entity[];
}

export interface Entity {
  value: string;
  metaData?: object;
}